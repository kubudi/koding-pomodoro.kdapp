koding-pomodoro
===============

What
-----
<i>koding-pomodoro</i> is an koding app that helps you to organize your daily workflow based on Pomodoro Technique.


Why
----
Organizing your tasks and managing your time efficiently is always a hard problem to solve. 

For example:
*  Your day may seem endless in the mornings. Thus you usually assign tasks to yourself which is much more than you can manage. 

*  you can misestimate how long a task will last to finish. 

*  you can easily distracted by several causes(facebook, phone, coworker)

Similar problems naturally decrease performance.

Pomodoro Technique leads you to estimate your tasks precisely, 
isolate yourself from outer world for a time period,
organise your daily tasks, 
and work in order, with efficiency.

Since
<a href="https://koding.com/">_koding.com_</a>
is a social development platform and aims to improve coding experience, we thougth it would be nice to have an tool to help people work better.

How
---
This project developed with _KDFramework_, on a _web browser_, at
<a href="https://koding.com/">_koding.com_</a>
(which is also writtin in KDFramework).

With
-----
* <a href="https://github.com/koding/docs">Koding</a>
* <a href="http://pomodorotechnique.com/">Pomodoro technique</a>
* KD Framework(hope a link will be there soon)
* <a href="http://coffeescript.org/">CoffeeScript</a>
* <a href="http://keith-wood.name/countdown.html">Countdown.js</a>