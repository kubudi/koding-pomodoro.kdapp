Veri modelleri
==============

Uygulamanin veri kaydetme ihtiyaci AppStorage ile karsilanmaktadir.
AppStorage verileri key-value esleri seklinde NOSQL bir veritabanina kaydetmeye olanak saglar.

Task:
----
Activity Inventory(AI) ve Todo Today(TT) icinde tutulan her isler task olarak adlandirilir.

####Alanlar:

-  id
-  createDate[Date]     : olusturuldugu gun
-  date[Date]           : Yapildigi gun. Task TT ye tasingidinda setlenir. Eger null ise Task AI de demektir
-  time[Date]           : Yapilacagi saat (Optional. nullable)
-  desc[String]         : Taskin tanimi
-  estimate[Int]        : Taski tamamlamak icin tahmin edilen pomodoro sayisi
-  actual[Int]          : Taskin tamamlandigi pomodoro sayisi
-  external[Int]        : External Interruption sayisi
-  internal[Int}        : Internal Interruption sayisi
-  startDate[Date]      : Sayacin basladigi tarihi tutar.
-  finishDate[Date]     : Sayacin bittigi tarihi tutar.
-  isSaved[Boolean]     : Pomodoro bitislerinde kisi ilgili estimate i isaretleyerek o pomodoro ile ilgili verilerin kaydedilmesini tetikler.
-  isPomodoro[Boolean]  : Bu field sayacin pomodoro sayaci olup olmadigini tutar.

####Metodlar:

-  insert                     : Storage'a yeni task ekler
-  update()                   : Storage da ilgili itemi gunceller.
-  remove()                   : Storagedan ilgili itemi siler
-  getById(id,callback)       : Verilen id ye ait olan tasklari getirir.
-  getByDate(date, callback)  : Verilen tarihe gore o tarihe esit olan tasklari getirir.
-  getToday[List[Task]]       : Yapildigi gun(date) verilen tarihe esit olan tasklari dondurur.
-  getAll(callback)           : Tum tasklari dondurur
-  isActive()[Boolean]        : Sayac calisiyor mu(start date setlenmis mi)
-  activate()                 : Sayaci aktif etmeye yarar
-  disactivate()              : sayaci deaktif etmeye yarar
-  getActive(clb)             : Statik bir methoddur. Aktif durumdaki sayac ogesini getirir. Icine aldigi callback parametresi sayesinde islemler yapilabilir.
