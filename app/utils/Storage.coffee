#This class is used for appStorage actions.
#appStorage is a key-value type NOSQL database.
class Storage
    constructor: ->
        super
    
    #Save item to the storage
    Storage.save = (key, item) ->
        appStorage.fetchStorage ()->
            items = appStorage.getValue key
            items or= []
            items.push item
            appStorage.setValue key, items

    #Remove item from storage
    Storage.remove = (key, id) ->
        appStorage.fetchStorage ()->
            items = appStorage.getValue key
            for i in items
                if(i.id == id)
                    items.splice(items.indexOf(i), 1)
                    console.log("removed: ")
                    break
            appStorage.setValue key, items

    #Remove all items
    Storage.removeAll = (key) ->
        appStorage.fetchValue key, (items) ->
            appStorage.setValue key, []
