#Modify Counter buttons.(enable-disable)  
buttonSettings = ->
    if CounterController.selectedTask
        CounterCpanel.buttons.Stop.enable()
        CounterCpanel.buttons.Start.disable()
    else
        CounterCpanel.buttons.Stop.disable()
        CounterCpanel.buttons.Start.enable()

#Get Todo Today elements from storage.
getTT = ->
    Task.getToday (tasks)->
        if tasks
            for task in tasks
                tw = TTController.addItem task , 0,  addTaskAnimation
                
                #If there is an active pomodoro, restore it.
                if task.isActive()
                    CounterController.activate(tw)
                    now = new Date()
                    finSecs = task.finishDate.getMinutes() * 60 + task.finishDate.getSeconds()
                    nowSecs = now.getMinutes() * 60 + now.getSeconds()
                    secs = (finSecs - nowSecs) % 60
                    mins = (finSecs - nowSecs - secs) / 60  
                    CounterController.pomodoro(mins, secs, task.isPomodoro)

#Get Activity Inventory elements from storage.
getAI = ->
    Task.getAll (tasks)->
        if tasks
            for task in tasks
                if task.date == null
                    AIController.addItem task , 0,  addTaskAnimation
