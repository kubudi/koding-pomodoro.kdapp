#global configurations and variables.

#for console.log
KD.enableLogs()

#storages
appStorage = new AppStorage "tasks", "1.0"

#css animation
addTaskAnimation = type : "fadeIn", duration : 1000 # in msecs
