#This is a helper class which generate UID      
class UID
    constructor:->
        super
    
    #Generates new UID. Thanks to an awesome answer at stackoverflow, its quite fancy
    #http://stackoverflow.com/a/2117523/1023927
    UID.generate = ()->
        word = "xxxxxxxx-4xxx-yxxx"
        uid = word.replace(/[xy]/g, (c) ->
          r = Math.random() * 16 | 0
          v = (if c is "x" then r else (r & 0x3 | 0x8))
          v.toString 16
        )