#View of an single task element.
#ListViews contains this kind of elements.
class TaskView extends KDListItemView
  constructor: (options, data) ->
      
    options.cssClass = "taskView"  
    
    super
    
    @content = @getData()
    
    @time = @content.time
    if @content.time == null or @content.time == "" then  @time = "-"
    
    @estimates = new KDView cssClass : ""
     
    #checked checkbox as "actual" count
    #unchecked checkbox as "estimate - actual" count
    for i in [0...@content.est] 
        cb = new KDInputView
            type      : "checkbox"
            disabled   : yes
        cb.domElement[0].checked = (i < @content.actual)  #tmbaew
        @estimates.addSubView cb
        
    @buttons = new KDButtonGroupView
      buttons:
        #Delete Button
        "D" :
            callback  : =>
                TaskViewController.remove(this)
        #Edit Button
        "E" :
            callback  : =>
                TaskViewController.edit(@)
        #Move Button
        "M" :
            callback  : =>
                TaskViewController.move(this)
  
  #onClick event for list element.
  click: ->
    TaskViewController.click(this)
    
  pistachio: =>
    """
    <div >
        <span style="width:10%; display: inline-block; vertical-align:top; font-size:16px;">#{@time}</span>  
        <span style="width:55%; display: inline-block; vertical-align:top; font-size:16px;">#{@content.desc}</span> 
        <span style="width:15%; display: inline-block; vertical-align:top;">{{> @estimates}}</span> 
        <span style="width:10%; ">{{> @buttons}}</span>
    </div>
    """
    
  viewAppended: ->
      @setTemplate do @pistachio
