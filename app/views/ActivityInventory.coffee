#Activity Inventory List
class ActivityInventory extends JView

  constructor: (options = {}, data) ->
    
    options.cssClass = ""
    super options, data
    
    @name = new KDHeaderView
      type: "big"
      cssClass: "tableName"
      title: "Activity Inventory"
      
    @addTask = new KDButtonView
        title      : "+"
        style      : "small-gray"
        cssClass   : "addTask"
        callback   : ->
            NewTaskModal()
      
    @listView = AIController.getView()
    
    
  pistachio:->
    """
    <div style= "height:100%"> 
    {{> @name}}
    {{> @addTask}}
    {{> @listView}}
    """
    