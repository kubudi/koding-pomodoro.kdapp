#Todo Today List View
class TodoToday extends JView

  constructor: (options = {}, data) ->
    
    options.cssClass = ""
    
    super options, data
    
    @name = new KDHeaderView
      type: "big"
      cssClass: "tableName"
      title: "Todo Today"
      
    
    @listView = TTController.getView()
    
    
  pistachio:->
    """
    <div style= "height:100%"> 
    {{> @name}}
    {{> @listView}}
    """
