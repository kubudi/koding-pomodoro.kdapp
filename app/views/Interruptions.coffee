#Interruptions section
class Interruptions extends JView

    constructor: (options = {}, data) ->
    
        options.cssClass = ""
        super options, data
        
        
        @internalButton = new KDButtonView
            title      : "+"
            disabled   : true
            callback   : ->
                InterruptionsController.increase(0)
                    
        @internalCount = new KDHeaderView
            type: "medium"
            title: "0"
            
        @internal      = new KDSplitView
            cssClass      : ""
            type          : "vertical"
            resizable     : no
            sizes         : [ "40%", "15%", "5%", "40%" ]
            views         : [ 
                null,               #%40 blank on the left
                new KDHeaderView
                    type: "medium"
                    title: "Internal:", 
                @internalCount, 
                @internalButton
                
            ]
            
        @externalCount = new KDHeaderView
            type: "medium"
            title: "0"
            
        @externalButton = new KDButtonView
            title      : "+"
            disabled   : true
            callback   : ->
                InterruptionsController.increase(1)
                
        @external      = new KDSplitView
            cssClass      : ""
            type          : "vertical"
            resizable     : no
            sizes         : [ "40%", "15%", "5%", "40%" ]
            views         : [ 
                null,               #%40 blank on the left 
                new KDHeaderView
                    type: "medium"
                    title: "External:", 
                @externalCount, 
                @externalButton
            ]
        
        @interruptions    = new KDSplitView
            cssClass      : ""
            type          : "horizontal"
            resizable     : no
            sizes         : [ "8%", "8%" ]
            views         : [ @internal, @external]
    
    #All the elements created seperately and gathered together with splitViews.
    pistachio:->
      """
        {{> @interruptions}} 
        
      """