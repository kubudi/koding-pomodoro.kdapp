#Counter buttons. This is seperate for being able to reach by Controllers  
CounterCpanel = new KDButtonGroupView
      buttons:
        "Start" :
            callback  : =>
               CounterController.start(25, 0, "POMODORO BASLADI", true)

        "Stop" :
            disabled  : true
            callback  : =>
                CounterController.stop()
                CounterController.selectedTask = null
        
        