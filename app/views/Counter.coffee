#Counter at the right top of the screen
class Counter extends JView

    constructor: (options = {}, data) ->
    
        options.cssClass = ""
        
        super options, data
        
        #Counter buttons
        @cpanel = CounterCpanel
    
    #the div with ``timer`` id is our counter. countdown.js will do the rest.
    pistachio:->
        """
            <div class='counter'>
                <div class="countDown">
                    <div id='timer'>00:00</div>
                    <div class="counterButtons">{{> @cpanel}}</div>
                </div>
            </div>
            
        """
    