#Modal view for creating new Task, 
#parameters: task =
#task = null:  New Task
#task != null: Update Task 
NewTaskModal = (task)->
    @defData = task?.data
    
    #This is a modal view. It contains task submission form 
    modal = new KDModalViewWithForms
        title     : "New Task"
        content   : ""
        overlay   : yes
        width     : 500
        height    : "auto"
        cssClass  : ""
        tabs                    :
            navigable             : yes
            forms                 :
                #Task submission. 
                "NewTask"           :
                    buttons           :
                        Save            :
                            title         : "Save"
                            style         : ""
                            type          : "submit"
                            callback      : ->
                                NewTaskController.save(modal, task)
                        Cancel          :
                            title         : "Cancel"
                            style         : ""
                            type          : "cancel"
                            callback      : ->
                                modal.destroy()
                    fields            :
                        Time            :
                            label         : "Time"
                            type          : "time"
                            name          : "time"
                            placeholder   : ""
                            defaultValue  : @defData?.time
                        Description     :
                            label         : "Description  *"
                            type          : "text"
                            name          : "desc"
                            placeholder   : "A brief summary."
                            defaultValue  : @defData?.desc
                            validate      :
                               rules       :
                                    required  : yes
                               messages    :
                                    required  : "description required!"
                        Estimate        :
                           label         : "Estimate  *"
                           type          : "number"
                           name          : "est"
                           placeholder   : "Approximate estimate"
                           defaultValue  : @defData?.est
                           validate      :
                                rules       :
                                    required  : yes
                                messages    :
                                    required  : "estimate required!"
    