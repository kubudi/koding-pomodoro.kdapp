#This is the main frame. Every element is a subview of MainView
#Elements organised with KDSplitView elements
class MainView extends JView

  constructor: (options = {}, data) ->
    super options, data
    
    
    @container          = new KDView                cssClass : ""
    @todoToday          = new TodoToday             delegate : @
    @activityInventory  = new ActivityInventory     delegate : @
    @counter            = new Counter               delegate : @
    @interruptions      = InterruptionsController.interruptionsView
    
    #inits
    getTT()
    getAI()
    buttonSettings()

    #tables 
    @leftSide     = new KDSplitView
      cssClass      : ""
      type          : "horizontal"
      resizable     : no
      sizes         : [ "50%", "50%" ]
      views         : [@todoToday, @activityInventory]
    
    #counter and interruptions
    @rigthSide      = new KDSplitView
      cssClass      : ""
      type          : "horizontal"
      resizable     : no
      sizes         : [ "30%", null ]
      views         : [ @counter, @interruptions]
    
    #whole app
    @mainStage      = new KDSplitView
      cssClass      : ""
      type          : "vertical"
      resizable     : no
      sizes         : [ "60%", null ]
      views         : [ @leftSide, @rigthSide]
      
    @container.addSubView @mainStage
  
  pistachio: ->
    """
    <h2>Hello</h2>
    {{> @container}}
    """
  
