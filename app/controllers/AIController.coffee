#Activity Inventory Controller.
#This is a list Controller. It's used for adding, removing etc. to a List View(Activity Inventory List for instance)
AIController = new KDListViewController
  lastToFirst         : no
  startWithLazyLoader : no
  scrollView          : yes
  viewOptions         :
    type              : "example-list"
    itemClass         : TaskView
    