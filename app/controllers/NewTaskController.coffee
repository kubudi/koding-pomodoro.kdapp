#constructor for newTask view
class NewTaskController
    
    constructor: () ->
        #empty
    
    #Save the task to storage unless form data is invalid
    @save : (modal, task) ->
        #Submitting task provides to validate fields with given rules
        modal.modalTabs.forms.NewTask.submit()
        
        #If validation is passed
        if modal.modalTabs.forms.NewTask.valid
            formData = modal.modalTabs.forms.NewTask.getData()
            current = if task?.data.date then TTController else AIController
            if task?
                current.removeItem task
                task?.data.time = formData.time
                task?.data.desc = formData.desc
                task?.data.est = formData.est
                Task.getById(task?.data.id,(obj)->
                    if obj != null
                        obj.time = formData.time
                        obj.desc = formData.desc
                        obj.est = formData.est
                        obj.update()
                    )
                current.addItem task?.data, 0
            else
                newTask = new Task(formData.time, formData.desc, formData.est)
                newTask.insert()
                current.addItem newTask , 0,  addTaskAnimation 
            modal.destroy() 
