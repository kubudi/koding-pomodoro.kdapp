#Interruptions section is managed by this controlled.
class InterruptionsController
    
    constructor: () ->
        #empty
    
    #internal and external interruptions count    
    @extCount = 0
    @intCount = 0
    
    #Controller holds an instance of Interruptions view. When required, this instance is used at views.
    #The reason for creating instance here is to make it reachable by controller. 
    @interruptionsView = new Interruptions
    
    
    @disable: ->
        @interruptionsView.internalButton.disable()
        @interruptionsView.externalButton.disable()

    
    @enable: ->
        @interruptionsView.internalButton.enable()
        @interruptionsView.externalButton.enable()
    
    #Clear data and disable buttons.
    @reset: ->
        @extCount = 0
        @intCount = 0
        @interruptionsView.internalCount.updateTitle(0)
        @interruptionsView.externalCount.updateTitle(0)
        @disable()
    
    #increase interruption count:    
    # e : interruption type
    # 0: internal, 1: external
    @increase: (e) ->
        taskData = CounterController.selectedTask.data
        if e == 0 
            @interruptionsView.internalCount.updateTitle(++@intCount)
            taskData.internal = @intCount 
        else if e == 1
            @interruptionsView.externalCount.updateTitle(++@extCount)
            taskData.external = @extCount
        taskData.update()
    
    #Sets Interruption labels.
    # i: internal interruption count
    # e: external interruption count
    @set: (i, e) ->
        @intCount = i
        @interruptionsView.internalCount.updateTitle(i)
        @extCount = e
        @interruptionsView.externalCount.updateTitle(e)
