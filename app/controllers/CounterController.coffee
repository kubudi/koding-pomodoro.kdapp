#This controller helps to manage counter. All actions for counter are made by that controller.
class CounterController
    constructor: () ->
        
    #As the name implies, selected task. That indicates which task is on progress during pomodoro.
    @selectedTask = null
    
    #To be honest, it's not activating anything. Pomodoro start button looks for selectedTask is assigned.
    #By assigning it, you're indirectly activating the start butoon.
    @activate: (task) ->
        CounterController.selectedTask?.domElement.removeClass("selectedTask")
        task.domElement.addClass("selectedTask")
        CounterController.selectedTask = task
        InterruptionsController.set(task.data.internal, task.data.external)
        
    #Start pomodoro
    #message is not in use
    @start: (minutes, seconds, message, is_pomodoro_time) ->
        if !CounterController.selectedTask
            new KDNotificationView
                title : "Please select a task first" 
        else if CounterController.selectedTask.data.actual == CounterController.selectedTask.data.est 
            new KDNotificationView
                title : "Selected task has finished already" 
        else
            CounterController.selectedTask.data.activate(is_pomodoro_time)
            @pomodoro(minutes, seconds, is_pomodoro_time)
        
    #Stop pomodoro. If pomodoro is terminated. Everything resets
    @stop: ->
        
        CounterController.selectedTask.buttons.buttons.D.enable()
        CounterController.selectedTask.buttons.buttons.E.enable()
        CounterController.selectedTask.buttons.buttons.M.enable()
        
        CounterCpanel.buttons.Stop.disable()
        CounterCpanel.buttons.Start.enable()
        
        InterruptionsController.reset()
        
        $('#timer').countdown('destroy')
        $('#timer').text("00:00")
        
        CounterController.selectedTask?.data.deactivate()
        
        CounterController.selectedTask?.domElement.removeClass("selectedTask")
        
    #End of pomodoro. After a successfull pomodoro time, views reset, storage is updated.
    #A notification shown to the user. He/she can take a break if desired. 
    @finished: -> 
        x = confirm "Pomodoro finished! Do you want a break?"
        
        CounterCpanel.buttons.Stop.disable()
        CounterCpanel.buttons.Start.enable()
        
        taskData = CounterController.selectedTask?.data
        taskData.actual  += 1
        taskData.update()
        
        #check estimate box        
        for i in [0...taskData.actual] 
            CounterController.selectedTask.estimates.subViews[i].domElement[0].checked = true #tmbaew
        
        CounterController.stop()
        if x
            setTimeout (->
                CounterController.start(5,0,"Coffee Time!", false)
            ), 10
    
    #Starts a pomodoro counter
    @pomodoro: (minutes, seconds, is_pomodoro_time)->
        
        CounterCpanel.buttons.Stop.enable()
        CounterCpanel.buttons.Start.disable()
        
        if is_pomodoro_time
            InterruptionsController.enable()
            
            CounterController.selectedTask.buttons.buttons.D.disable()
            CounterController.selectedTask.buttons.buttons.E.disable()
            CounterController.selectedTask.buttons.buttons.M.disable()
        #
        
        until_ = new Date()
        until_.setMinutes(until_.getMinutes() + minutes)
        until_.setSeconds(until_.getSeconds() + seconds)
        $("#timer").countdown
            alwaysExpire: true,
            until: until_,
            format: "MS",
            compact: true,
            onExpiry: if is_pomodoro_time then CounterController.finished else CounterController.stop
            
