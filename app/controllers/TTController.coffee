#Todo Today Controller.
#This is a list Controller. It's used for adding, removing etc. to a List View(Todo Today List for instance)
TTController = new KDListViewController
  lastToFirst         : no
  startWithLazyLoader : no
  selection           : yes
  scrollView          : yes
  viewOptions         :
    type              : "example-list"
    itemClass         : TaskView
    