#Controller for TaskView
class TaskViewController

    constructor : ->
        #empty
    
    #remove given task from views and storage    
    @remove : (task) ->
        content = task.getData()
        if content.date
            TTController.removeItem task
        else
            AIController.removeItem task
        content.remove()
    
    #Open a new dialog for editing with current values of task
    @edit : (task) ->
        NewTaskModal(task)
        
    #Move task to other table (between the lists AI <-> TT)
    @move: (task) ->
        content = task.getData()
        if content.date
            content.date = null
            AIController.addItem content , 0,  addTaskAnimation 
            TTController.removeItem task
            if CounterController.selectedTask == task
                CounterController.selectedTask = null
        else
            content.date = new Date
            TTController.addItem content , 0,  addTaskAnimation 
            AIController.removeItem task
        content.update()

    @click : (task) ->
        content = task.getData()
        Task.getActive (counter) ->
            if content.date && counter == null
                CounterController.activate(task)