#Task modal
class Task 
    
    constructor: (@time, @desc, @est) ->
      
        #See DataModels for further explanation      
        @id = UID.generate()
        @date = null
        @createDate = new Date()
        @actual = 0
        @external = 0
        @internal = 0
        @startDate = null
        @finishDate = null
        @isSaved = true
        @isPomodoro = true
        
    isActive : ->
        now = new Date
        if @startDate && @finishDate
            if now > @startDate && now < @finishDate
                true
            else
                @deactivate()
                false
        else
            false
    
    update : ->
        item = this
        uid = @id
        appStorage.fetchStorage (storage) ->
            tasks = appStorage.getValue "tasksV1"
            tasks or= []
            for i in [0...tasks.length]
                if tasks[i].id == uid
                    tasks[i] = item
                    appStorage.setValue "tasksV1", tasks
            
    insert : ->
        item = this
        Storage.save("tasksV1",item)
        
    remove : ->
        uid = @id
        appStorage.fetchStorage (storage) ->
            Storage.remove("tasksV1", uid)
                
    activate : (is_pomodoro_time)->
        @isPomodoro = is_pomodoro_time
        finish = new Date
        if @isPomodoro 
            finish.setMinutes(finish.getMinutes()+25)
        else 
            finish.setMinutes(finish.getMinutes()+5)
        @startDate = new Date
        @finishDate = finish
        @update()
                
    deactivate : ->
        @isSaved    = true
        @startDate  = null
        @finishDate = null
        @update()
    
    @apply: (obj)->
        if obj
            task = new Task(obj.time, obj.desc, obj.est)
            task.id = obj.id
            task.date = if obj.date then new Date(obj.date) else null
            task.createDate = new Date(obj.createDate)
            task.actual = obj.actual
            task.external = obj.external
            task.internal = obj.internal
            task.startDate = if obj.startDate then new Date(obj.startDate) else null
            task.finishDate = if obj.finishDate then new Date(obj.finishDate) else null
            task.isSaved = obj.isSaved
            task.isPomodoro = obj.isPomodoro
            
            task
        else
            null
            
    @getById: (id,callback)->
        appStorage.fetchStorage (storage) ->
            tasks = appStorage.getValue "tasksV1"
            tasks or= []
            obj = null
            for item in tasks
                if item.id == id then obj = item
            callback(obj)
    
    @getByDate: (date, callback)->
        appStorage.fetchStorage (storage) ->
            tasks = appStorage.getValue "tasksV1"
            tasks or= []
            arr = []
            for item in tasks
                newDate = new Date(item.date)
                if newDate.toDateString() == date.toDateString()
                    task = Task.apply(item)
                    arr.push task
            callback(arr)
    
    @getAll: (callback)->
        appStorage.fetchStorage (storage) ->
            tasks = appStorage.getValue "tasksV1"
            tasks or= []
            arr = []
            for item in tasks
                task = Task.apply(item)
                arr.push task
            callback(arr)
    
    @getToday: (clb)->
        @getByDate(new Date, clb)
        
    @getActive: (callback)->
        appStorage.fetchStorage (storage) ->
            tasks = appStorage.getValue "tasksV1"
            tasks or= []
            active = null
            for item in tasks
                if item.startDate && item.finishDate
                    task = Task.apply(item)
                    if task.isActive()
                        active = task
                
            callback(active)
    